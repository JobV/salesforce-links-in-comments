from __future__ import print_function
import gitlab
import re
import pandas as pd
import time

projects = ['gitlab-org/gitlab-ee', 'gitlab-org/gitlab-ce']
project_name = 'gitlab-org/gitlab-ee'

for project_name in projects:
    gl = gitlab.Gitlab('https://gitlab.com',
                       private_token='PRIVATETOKENHERE')
    project = gl.projects.get(project_name)

    issues = project.issues.list(order_by='updated_at',
                                 sort='desc', per_page=100, page=1)

    # initialize variables
    df = pd.DataFrame(columns=['issue', 'sfdc links'])
    df['sfdc links'] = df['sfdc links'].astype('int')
    i = 0

    generator = project.issues.list(as_list=False, per_page=100)
    number_of_pages_issues = generator.total_pages
    print(number_of_pages_issues)

    for j in range(1, number_of_pages_issues):
        print(j)
        issues = project.issues.list(order_by='updated_at',
                                     sort='desc', per_page=100, page=j)
        for issue in issues:
            notes = issue.notes.list(all=True)
            for note in notes:
                searchObj = re.search(
                    r'(na34\.salesforce\.com\/[^ ]*)', note.body, re.I)

                # if there is one or more salesforce links
                if searchObj:
                    df.loc[i] = [issue.web_url, len(searchObj.groups())]
                    i = i + 1

    # once done, summarize data
    done = df['sfdc links'].groupby(df['issue']).sum()
    print(done)

    filename = 'sfdc_' + \
        project_name.replace('/', '') + str(int(time.time())) + '.csv'
    done.to_csv(filename)
